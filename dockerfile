FROM node as build
WORKDIR /gatsby
COPY . .
RUN npm install -g gatsby-cli 
RUN npm install
RUN gatsby build
# ENTRYPOINT [ "gatsby", "develop", "-H", "0.0.0.0" ]

FROM nginx 
COPY --from=build /gatsby/public /usr/share/nginx/html
ENTRYPOINT [ "nginx", "-g", "daemon off;" ]